import asyncio
import tornado.websocket
from usb2snes import usb2snes, IPS
import json
import logging
import serial.tools.list_ports
from typing import List


MAX_MESSAGE_SIZE = 1024


class BaseMessageHandler(object):
    def _get_block_size(self):
        return 64 if self.flags & usb2snes.Flags.DATA64B else 512

    async def _pad_to_block_size(self, size):
        block_size = self._get_block_size()
        if size % block_size != 0:
            self.ws.device.writer.write(b"\x00" * (block_size - (size % block_size)))
        await self.ws.device.writer.drain()

    async def _chunked_transfer(self, data):
        self.ws.device.writer.write(data)
        await self._pad_to_block_size(len(data))

    async def _safe_transfer_to_usb(self, size, callback=None):
        """
        Transfer from websocket directly to USB (hopefully) without crashing the USB interface.

        The USB interface is a bit picky after you tell it you're going to send bulk data.  If you don't actually follow
        up on that promise, the USB interface will just stall forever.  Additionally, you need to be sure that you're
        actually providing a multiple of the data block size (512 or 64 depending on flags).

        Because of these few caveats, this function exists to (attempt to) ensure that the USB interface won't crash. In
        the event that not enough data is available on the websocket interface, we simply pad the reamining data with 
        0's.  This is likely not the "right" thing to do, but it ensures we don't hold up the USB interface indefinitely.
        This method also takes care of padding 0's to reach the block size (as is required by the protocol)
        """
        put_bytes = 0
        try:
            while put_bytes < size:
                chunk = await asyncio.wait_for(self.ws.message_queue.get(), 30)
                # TODO: Handle *too much* data, write partial chunk
                self.ws.device.writer.write(chunk)
                await self.ws.device.writer.drain()
                put_bytes += len(chunk)

                if callback:
                    callback(put_bytes)
        except asyncio.TimeoutError:
            missing_bytes = size - put_bytes
            logging.error("Waited 30 seconds, but didn't get enough data to send. Padding %d bytes of 0", missing_bytes)
            self.ws.device.writer.write(b"\x00" * missing_bytes)

        await self._pad_to_block_size(size)

    def __init__(self, ws, server_space, flags, opcode):
        self.ws = ws
        self.server_space = usb2snes.ServerSpace[server_space] if server_space else usb2snes.ServerSpace.SNES
        self.flags = usb2snes.Flags[flags] if flags else usb2snes.Flags.NONE
        self.opcode = opcode

    async def __call__(self, *args, **kwargs):
        raise NotImplementedError(f"Message handler {self.__class__.__name__} must implement __call__")


class MessageHandlerName(BaseMessageHandler):
    async def __call__(self, name):
        self.ws.name = name


class MessageHandlerDeviceList(BaseMessageHandler):
    async def __call__(self, *args, **kwargs):
        devices = [d.device for d in serial.tools.list_ports.grep('sd2snes')]

        # if self.ws.usb2snes.reader is not None:
        #     devices.append("FakeCOM")
        self.ws.send_results(devices, True)


class MessageHandlerAttach(BaseMessageHandler):
    async def __call__(self, port, *args, **kwargs):
        self.ws.port = port
        self.ws.device = await self.ws.devices.get_or_add(port)


class MessageHandlerInfo(BaseMessageHandler):
    async def __call__(self, *args, **kwargs):
        info = await self.ws.device.info()
        self.ws.send_results(info, True)


class MessageHandlerAppVersion(BaseMessageHandler):
    async def __call__(self, *args, **kwargs):
        self.ws.send_results(["PyShim"], True)


class MessageHandlerList(BaseMessageHandler):
    async def __call__(self, *directories, **kwargs):
        for directory in directories:
            results = await self.ws.device.list_files(directory)
            data = []
            for f in results:
                data.append(f[0])
                data.append(f[1])
            self.ws.send_results(data, True)


class MessageHandlerBoot(BaseMessageHandler):
    async def __call__(self, path, **kwargs):
        await self.ws.device.boot_rom(path)


class MessageHandlerGetAddress(BaseMessageHandler):
    async def __call__(self, *args, **kwargs):
        can_vectorize = len(args) < 16 and all([int(x, 16) < 255 for x in args[1::2]])
        total_size = sum([int(x, 16) for x in args[1::2]])
        async with self.ws.device.device_lock:
            if can_vectorize:
                await self.ws.device.send_command(
                    usb2snes.Opcode.VGET,
                    self.server_space,
                    usb2snes.Flags.NORESP | usb2snes.Flags.DATA64B,
                    *[usb2snes.GetSpecifier(int(args[i], 16), int(args[i+1], 16)) for i in range(0,len(args),2)]
                )
                recvd = 0
                send_buf = b""
                while recvd < total_size:
                    res = await self.ws.device.reader.readexactly(64)
                    recvd += len(res)

                    send_buf += res
                    if len(send_buf) > 1024:
                        self.ws.write_message(send_buf[:1024], True)
                        send_buf = send_buf[1024:]
                self.ws.write_message(send_buf, True)
            else:
                address = int(args[0], 16)
                length = int(args[1], 16)
                await self.ws.device.send_command(
                    usb2snes.Opcode.GET,
                    self.server_space,
                    usb2snes.Flags.NORESP,
                    address, length
                )
                read = 0
                adjusted_length = length
                if adjusted_length % 512 != 0:
                    adjusted_length += (512 - (adjusted_length % 512))
                while read < adjusted_length:
                    data = await self.ws.device.reader.readexactly(min(adjusted_length - read, 1024))
                    read += len(data)
                    self.ws.write_message(data, True)


class MessageHandlerReset(BaseMessageHandler):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.opcode = usb2snes.Opcode.MENU_RESET if self.opcode == 'Menu' else usb2snes.Opcode.RESET

    async def __call__(self, *args):
        async with self.ws.device.device_lock:
            await self.ws.device.send_command(
                self.opcode,
                self.server_space,
                self.flags,
                *args
            )


class MessageHandlerGetFile(BaseMessageHandler):
    async def __call__(self, *files):
        for f in files:
            async with self.ws.device.device_lock:
                size = await self.ws.device.send_command(usb2snes.Opcode.GET, usb2snes.ServerSpace.FILE, self.flags, f)
                self.ws.send_results([f"{size:x}"], size == 0)

                block_size = self._get_block_size()
                read_size = size
                if read_size % block_size != 0:
                    read_size += (block_size - (read_size % block_size))

                read_byte = 0
                while read_byte < read_size:
                    chunk = await self.ws.device.reader.readexactly(min(read_size - read_byte, 1024))
                    read_byte += len(chunk)
                    if read_byte > size:
                        chunk = chunk[:block_size-(read_byte-size)]
                    self.ws.write_message(chunk, True)


class MessageHandlerPutFile(BaseMessageHandler):

    async def __call__(self, *files):
        if len(files) % 2 != 0:
            return

        for i in range(0, len(files), 2):
            name = files[i]
            size = int(files[i+1], 16)
            if size == 0:
                continue

            async with self.ws.device.device_lock:
                await self.ws.device.send_command(
                    usb2snes.Opcode.PUT,
                    usb2snes.ServerSpace.FILE,
                    self.flags,
                    name, size
                )

                await self._safe_transfer_to_usb(size)


class MessageHandlerPutFileAck(MessageHandlerPutFile):
    async def __call__(self, *files):
        if len(files) % 2 != 0:
            return

        for i in range(0, len(files), 2):
            name = files[i]
            size = int(files[i+1], 16)
            if size == 0:
                continue

            async with self.ws.device.device_lock:
                await self.ws.device.send_command(
                    usb2snes.Opcode.PUT,
                    usb2snes.ServerSpace.FILE,
                    self.flags,
                    name, size
                )

                await self._safe_transfer_to_usb(
                    size,
                    lambda put_bytes: self.ws.send_results([name, put_bytes], put_bytes >= size)
                )


class MessageHandlerRemove(BaseMessageHandler):
    async def __call__(self, path, **kwargs):
        await self.ws.device.delete_file(path)


class MessageHandlerMakeDir(BaseMessageHandler):
    async def __call__(self, path, **kwargs):
        await self.ws.device.make_directory(path)


class MessageHandlerPutIPS(BaseMessageHandler):
    async def __call__(self, name, size, **kwargs):
        size = int(size, 16)
        ips = b""

        while len(ips) < size:
            ips += await self.ws.message_queue.get()

        ips = IPS.parse(ips)

        async with self.ws.device.device_lock:
            for patch_num, patch in enumerate(ips.items):
                flags = self.flags | usb2snes.Flags.NORESP
                if name == "hook":
                    if patch_num == 0:
                        flags |= usb2snes.Flags.CLRX
                    elif patch_num == (len(ips.items) - 1):
                        flags |= usb2snes.Flags.SETX

                await self.ws.device.send_command(
                    usb2snes.Opcode.PUT,
                    self.server_space,
                    flags,
                    patch.address, len(patch.data)
                )
                print(f"Address: {patch.address:x}, bytes: {len(patch.data)}")
                
                await self._chunked_transfer(patch.data)
        print("Done patching")


class MessageHandlerPutAddress(BaseMessageHandler):
    async def __call__(self, *args, **kwargs):
        can_vectorize = len(args) < 16 and all([int(x, 16) < 255 for x in args[1::2]])
        total_size = sum([int(x, 16) for x in args[1::2]])

        self.flags |= usb2snes.Flags.NORESP

        async with self.ws.device.device_lock:
            if can_vectorize:
                self.flags |= usb2snes.Flags.DATA64B
                await self.ws.device.send_command(
                    usb2snes.Opcode.VPUT,
                    self.server_space,
                    self.flags,
                    *[usb2snes.GetSpecifier(int(args[i], 16), int(args[i+1], 16)) for i in range(0,len(args),2)]
                )

                await self._safe_transfer_to_usb(total_size)
            else:
                for i in range(0, len(args), 2):
                    address = int(args[i], 16)
                    length = int(args[i+1], 16)
                    await self.ws.device.send_command(
                        usb2snes.Opcode.PUT,
                        self.server_space,
                        self.flags,
                        address, length
                    )

                    await self._safe_transfer_to_usb(total_size)


# TODO: Python magic to lookup
MESSAGE_HANDLERS = {
    "Name": MessageHandlerName,
    "DeviceList": MessageHandlerDeviceList,
    "Attach": MessageHandlerAttach,
    "Info": MessageHandlerInfo,
    "AppVersion": MessageHandlerAppVersion,
    "List": MessageHandlerList,
    "Boot": MessageHandlerBoot,
    "GetAddress": MessageHandlerGetAddress,
    "Menu": MessageHandlerReset,
    "Reset": MessageHandlerReset,
    "GetFile": MessageHandlerGetFile,
    "PutFile": MessageHandlerPutFile,
    "PutFileAck": MessageHandlerPutFileAck,
    "Remove": MessageHandlerRemove,
    "MakeDir": MessageHandlerMakeDir,
    "PutIPS": MessageHandlerPutIPS,
    "PutAddress": MessageHandlerPutAddress,
}

# TODO: Refactor to allow multiple users
class Usb2SnesMessageHandler(object):
    def __init__(self, ws: tornado.websocket.WebSocketHandler, usb2snes_: usb2snes.Usb2Snes, devices):
        self.ws = ws
        self.usb2snes = usb2snes_
        self.devices = devices
        self.device = None
        self.message_queue = asyncio.Queue()

    async def run(self):
        try:
            while True:
                message = await self.message_queue.get()

                data = json.loads(message)
                print(f"Got message: {data}")
                try:
                    cls = MESSAGE_HANDLERS[data['Opcode']]
                    this_msg = cls(self, data.get('Space', None), data.get('Flags', None), opcode=data['Opcode'])
                    operands = data.get('Operands', None) or []
                    await this_msg(*operands)
                except KeyError:
                    logging.exception("Can't handle message")
                    self.ws.close()
                    break
        except asyncio.CancelledError:
            print(f"{self.__class__.__name__} run cancelled")
        except tornado.websocket.WebSocketClosedError:
            logging.exception("Websocket closed while processing message")
        except:
            logging.exception(f"Unknown error in {self.__class__.__name__}")
        if self.device is not None:
            del self.device

    def write_message(self, *args, **kwargs):
        self.ws.write_message(*args, **kwargs)

    def send_results(self, results: List[str], done: bool):
        res = {"Results": results, "Done": done}
        print(f"Sending results: {res}")
        self.write_message(res)

class Usb2SnesWebSocket(tornado.websocket.WebSocketHandler):
    def initialize(self, usb2snes_, devices, **kwargs):
        self.message_handler = Usb2SnesMessageHandler(self, usb2snes_, devices)
        self.message_task = None

    def check_origin(self, origin):
        return True

    def open(self):
        print("Opened")
        self.message_task = asyncio.ensure_future(self.message_handler.run())

    def on_message(self, message):
        self.message_handler.message_queue.put_nowait(message)

    def on_close(self):
        print("Closed")
        self.message_task.cancel()
        asyncio.ensure_future(self.message_task)
