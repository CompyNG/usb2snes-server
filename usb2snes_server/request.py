import base64
import tornado.web
import usb2snes


class BaseRequest(tornado.web.RequestHandler):
    def initialize(self, usb2snes_):
        self._usb2snes = usb2snes_

    async def __aenter__(self):
        return (await self._usb2snes.__aenter__())

    async def __aexit__(self, exc_type, exc_value, traceback):
        return (await self._usb2snes.__aexit__(exc_type, exc_value, traceback))


class Status(BaseRequest):
    def get(self):
        self.write({"ready": self._usb2snes.is_connected()})


class Files(BaseRequest):
    async def get(self, path):
        if not path.startswith('/'):
            path = "/" + path

        async with self:
            files = await self._usb2snes.list_files(path)

        self.write({"status": "OK", "files": files})

    async def put(self, path):
        if not path.startswith('/'):
            path = "/" + path

        async with self:
            await self._usb2snes.put_file(path, self.request.body)

    async def delete(self, path):
        if not path.startswith('/'):
            path = "/" + path

        async with self:
            await self._usb2snes.delete_file(path)

        self.write({"status": "OK"})


class MakeDir(BaseRequest):
    async def post(self, path):
        if not path.startswith("/"):
            path = "/" + path

        async with self:
            await self._usb2snes.make_directory(path)

        self.write({"status": "OK"})


class BootRom(BaseRequest):
    async def get(self, path):
        if not path.startswith("/"):
            path = "/" + path

        async with self:
            # TODO: Sanity check path?
            await self._usb2snes.boot_rom(path)

        self.write({"status": "OK"})


class FileDownload(BaseRequest):
    async def get(self, path):
        if not path.startswith('/'):
            path = "/" + path

        self.set_header('Content-Type', 'application/force-download')
        file_name = path.split('/')[-1]
        self.set_header('Content-Disposition', 'attachment; filename=\'{}\''.format(file_name))

        has_written_size_header = False
        async with self:
            async for chunk in self._usb2snes.download_file(path):
                if not has_written_size_header:
                    self.set_header("Content-Length", chunk[0])
                self.write(chunk[1])
                self.flush()

        self.finish()


class LivePatch(BaseRequest):
    async def put(self):
        ips_patch = usb2snes.IPS.parse(base64.b64decode(self.request.body.decode('utf-8')))
        async with self:
            await self._usb2snes.live_patch(ips_patch)

        self.write({"status": "OK"})


class StateMgmt(BaseRequest):
    async def get(self):
        async with self:
            data = (await self._usb2snes.save_state())

        self.set_header('Content-Type', 'application/force-download')
        # TODO: File name
        self.set_header('Content-Disposition', 'attachment; filename=state.ss0')
        self.write(data)

    async def put(self):
        data = self.request.body

        async with self:
            await self._usb2snes.restore_state(data)

        self.write({"status": "OK"})


class RamManagement(BaseRequest):
    async def get(self, address, length):
        async with self:
            data = await self._usb2snes.read_ram(int(address, 16), int(length))

        self.write({
            "status": "OK",
            "data": base64.b64encode(data).decode('utf-8')
        })


class WramManagement(BaseRequest):
    async def get(self, address, length):
        async with self:
            data = await self._usb2snes.read_wram(int(address, 16), int(length))

        self.write({
            "status": "OK",
            "data": base64.b64encode(data).decode('utf-8')
        })

    async def post(self, address):
        address = int(address, 16)
        data = base64.b64decode(self.request.body.decode('utf-8'))

        async with self:
            await self._usb2snes.write_wram(address, data)

        self.write({
            "status": "OK"
        })
