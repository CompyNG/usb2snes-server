#!/usr/bin/env python3
import asyncio
import os.path
import uvloop
import butter.inotify
import butter.asyncio.inotify
import tornado.web
import tornado.ioloop
from tornado.platform.asyncio import AsyncIOMainLoop
import usb2snes
from usb2snes_server.application import Usb2SnesServer


SD2SNES_DEVICE = "/dev/ttyACM0"


async def watch_device(u2s):
    try:
        if os.path.exists(u2s.device):
            await u2s.open()

        inotify = butter.asyncio.inotify.Inotify_async()
        wd = inotify.watch(os.path.dirname(u2s.device), butter.inotify.IN_CREATE | butter.inotify.IN_DELETE)

        while True:
            event = await inotify.get_event()
            if event.filename.decode('utf-8') == os.path.basename(u2s.device):
                if event.mask & butter.inotify.IN_CREATE:
                    await asyncio.sleep(0.1)
                    await u2s.open()
                if event.mask & butter.inotify.IN_DELETE:
                    u2s.close()
    except asyncio.CancelledError:
        u2s.close()
    except:
        traceback.print_exc()


if __name__ == "__main__":
    asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
    AsyncIOMainLoop().install()

    u2s = None
    # u2s = usb2snes.Usb2Snes(SD2SNES_DEVICE)
    # fut = asyncio.ensure_future(watch_device(u2s))
    fut = None

    app = Usb2SnesServer(u2s)
    app.listen(8080)

    try:
        print("Listening on :8080")
        asyncio.get_event_loop().run_forever()
    except KeyboardInterrupt:
        if fut is not None:
            if not fut.done():
                fut.cancel()
                asyncio.get_event_loop().run_until_complete(fut)
            u2s_watcher_exc = fut.exception()
            if u2s_watcher_exc is not None:
                print(u2s_watcher_exc)
