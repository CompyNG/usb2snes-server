var m = require("mithril");

import Menu from './Menu';

export default class Container {
    view(vnode) {
        return [
            m(Menu),
            // ApiHelper.loaded_percent !== null ?
            m('.conainer',
                m('.starter-template',
                    vnode.children
                )
            )
        ]
    }
}
