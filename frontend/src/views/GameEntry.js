"use strict";
var m = require("mithril");

export default class GameEntry {
    constructor(vnode) {
        this.gameList = vnode.attrs.gameList
        this.name = vnode.attrs.name
        this.deleting = false
        this.download_progress = null
    }

    view() {
        var deleting = this.deleting

        return m(`li.list-group-item${deleting ? ".list-group-item-danger" : ""}`, [
            m(`button.btn.btn-sm.btn-success${deleting ? ".disabled" : ""}`,
                { onclick: this.boot.bind(this) },
                m('span.oi.oi-play-circle')
            ),
            " ",
            m('span.oi.oi-file'),
            ` ${this.name}`,
            m('div.btn-group.float-right', [
                m(`button.btn.btn-sm.btn-danger${deleting ? ".disabled" : ""}`,
                    { onclick: this.remove.bind(this) },
                    m('span.oi.oi-trash')
                ),
                m(`button.btn.btn-sm.btn-primary${deleting ? ".disabled" : ""}`,
                    { onclick: this.download.bind(this) },
                    m('span.oi.oi-data-transfer-download')
                )
            ]),
            this.download_progress != null ?
                m('span.float-right', { style: "padding-right: 5px;padding-top: 5px" }, [
                    m('progress', { value: this.download_progress, max: 100 }),
                    " "
                ]) :
                "",
        ])
    }

    updateDownloadProgress(prg) {
        this.download_progress = prg * 100
        m.redraw()
    }

    download(e) {
        e.preventDefault()
        this.download_progress = 0
        m.redraw()
        return this.gameList.download(this.gameList.path, this.name, this.updateDownloadProgress.bind(this))
            .then((download_url) => {
                var a = document.createElement('a')
                a.style = "display: none"
                a.href = download_url
                a.download = this.name
                document.body.appendChild(a)
                a.click()
                URL.revokeObjectURL(download_url)
                this.download_progress = null
                m.redraw()
            })
    }

    boot() {
        return this.gameList.boot(`${this.gameList.path}/${this.name}`)
    }

    remove() {
        if (window.confirm(`Are you sure you want to delete ${this.name}?`)) {
            this.deleting = true
            return this.gameList.remove(`${this.gameList.path}/${this.name}`)
        }
    }
}
