"use strict";
var m = require("mithril");
var stream = require("mithril/stream")

import WramWrite from '../models/WramWrite';

export default class SmwCheats {
    constructor() {
        this.desired_item_box = stream(0)
        this.desired_powerup = stream(0)
        this.desired_lives = stream(10)
    }

    view() {
        return [
            m('h1', "SMW Cheats"),
            m('div.form-group.form-inline', [
                m('label[for=powerup]', "Powerup"),
                m('select.form-control.mx-sm-4#powerup',
                    { oninput: m.withAttr("value", this.desired_powerup), value: this.desired_powerup() },
                    this.constructor.POWERUPS.map(function(obj) {
                        return m(`option[value=${obj.key}]`, obj.value)
                    })
                ),
                m('button.btn.btn-primary', {
                    onclick: this.setPowerup.bind(this),
                }, "Set Powerup"),
            ]),
            m('div.form-group.form-inline', [
                m('label[for=item-box]', "Item Box"),
                m('select.form-control.mx-sm-4#item-box',
                    { oninput: m.withAttr("value", this.desired_item_box), value: this.desired_item_box() },
                    this.constructor.ITEM_BOX.map(function(obj) {
                        return m(`option[value=${obj.key}]`, obj.value)
                    })
                ),
                m('button.btn.btn-primary', {
                    onclick: this.setItemBox.bind(this),
                }, "Give Item")
            ]),
            m('div.form-group.form-inline', [
                m('label', "Exit Level"),
                m('button.btn.btn-primary.mx-sm-4',
                    { onclick: this.exitPrimary.bind(this) },
                    "Primary"
                ),
                m('button.btn.btn-primary',
                    { onclick: this.exitSecondary.bind(this) },
                    "Secondary"
                ),
            ]),
            m('div.form-group.form-inline', [
                m('button.btn.btn-primary',
                    { onclick: this.showSave.bind(this) },
                    "Show save prompt (when you move to next level tile)"
                ),
            ]),
            m('div.form-group.form-inline', [
                m('label[for=number-lives]', "Set number of lives"),
                m('input.form-control.mx-sm-4#number-lives[type=number]',
                    { oninput: m.withAttr("value", this.desired_lives), value: this.desired_lives() }
                ),
                m('button.btn.btn-primary', {
                    onclick: this.setLives.bind(this),
                }, "Set")
            ])
        ]
    }

    loadPatch() {
        return WramWrite.loadPatch().then(function(result) {
            if (!result) {
                alert("ERROR loading patch")
            }
        })
    }

    setPowerup() {
        WramWrite.write(0x19, parseInt(this.desired_powerup()))
    }

    setItemBox() {
        WramWrite.write(0xDC2, parseInt(this.desired_item_box()))
    }

    exitPrimary() {
        WramWrite.write(0x1493, 1)
    }

    exitSecondary() {
        WramWrite.write(0x1434, 1)
    }

    showSave() {
        WramWrite.write(0x13CA, 1)
    }

    setDesiredLives(value) {
        this.desired_lives = parseInt(value)
    }

    setLives() {
        WramWrite.write(0xDBE, parseInt(this.desired_lives()))
    }
}

SmwCheats.POWERUPS = [
    { key: 0, value: "None"},
    { key: 1, value: "Mushroom"},
    { key: 2, value: "Cape"},
    { key: 3, value: "Fire Flower"},
]
SmwCheats.ITEM_BOX = [
    { key: 0, value: "None" },
    { key: 1, value: "Mushroom" },
    { key: 2, value: "Fire Flower" },
    { key: 3, value: "Star" },
    { key: 4, value: "Feather" },
]
