"use strict";

var m = require('mithril');

var SnesWebsocket = {
    connected: false,
    websocket: null,
    message_callback: null,
    
    connect: function() {
        return new Promise((resolve, reject) => {
            var loc = window.location, new_uri
            if (loc.protocol === "https:") {
                new_uri = "wss:"
            } else {
                new_uri = "ws:"
            }
            new_uri += "//" + loc.host + "/api/v1/websocket/"

            this.websocket = new WebSocket(new_uri)
            this.websocket.onopen = (event) => {
                this.onopen(event)
                resolve()
            }
            this.websocket.onclose = (event) => {
                this.onclose(event)
                if (!this.connected) {
                    reject(event)
                }
            }
            this.websocket.onmessage = this.onmessage.bind(this)
        })
    },
    
    onopen: function(event) {
        console.log("Opened websocket")
        this.connected = true
        // this.websocket.send(JSON.stringify({
        //     "Opcode": "DeviceList",
        // }))
    },
    
    onmessage: function(event) {
        if (this.message_callback != null) {
            this.message_callback(event.data)
        }
    },
    
    onclose: function(event) {
        console.log("Websocket closed")
        if (this.connected) {
            location.reload()
        }
    },
    
    sendSingle: function(input, callback) {
        var self = this
        function finalizer() {
            var count = 0
            function complete() {
                if (--count === 0) {
                    m.redraw()
                }
            }

            return function finalize(promise) {
                var then = promise.then
                promise.then = function() {
                    count++
                    var next = then.apply(promise, arguments)
                    next.then(complete, function(e) {
                        complete()
                        if (count === 0) throw e
                    })
                    return finalize(next)
                }
                return promise
            }
        }

        var p = new Promise((resolve, reject) => {
            this.waitForMessageCallback().then(() => {
                this.message_callback = (event) => {
                    this.message_callback = null
                    resolve(JSON.parse(event))
                }
                this.send(input)
            })
        })

        return finalizer()(p)
    },
    
    send: function(input) {
        this.websocket.send(JSON.stringify(input))
    },
    
    waitForMessageCallback() {
        return new Promise((resolve, reject) => {
            var waitImpl = () => {
                if (this.message_callback == null) return resolve()
                setTimeout(waitImpl, 30)
            }
            waitImpl()
        })
    },
}

module.exports = SnesWebsocket
