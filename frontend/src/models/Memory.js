"use strict";
var m = require("mithril");

export default class Memory {
    constructor(address) {
        this.address = address
        this.data = new Uint8Array()
    }

    loadMemory() {
        var self = this

        // return m.request({
        //     method: "GET",
        //     url: 'http://localhost:8080/api/v1/memory/' + self.address,
        // }).then(function(result) {
        //     if (result.success) {
        //         self._base64ToArrayBuffer(result.data)
        //     }
        // })
    }

    _base64ToArrayBuffer(base64) {
        var binary_string = window.atob(base64)
        var len = binary_string.length
        this.data = new Uint8Array(len)
        for (var i = 0; i < len; ++i) {
            this.data[i] = binary_string.charCodeAt(i)
        }
    }
}
