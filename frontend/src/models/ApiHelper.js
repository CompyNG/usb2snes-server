"use strict";

var m = require('mithril');

function has(object, key) {
    return object ? hasOwnProperty.call(object, key) : false;
}

var ApiHelper = {
    loaded_percent: null,
    hide_loaded_timeout: null,
    
    request: function(params, callback) {
        return m.request(params).then(function(result) {
            if (!has(result, 'status') || result.status === "OK" || result.status === true) {
                return callback(result)
            }

            return false
        })
    },

    requestWithLoader: function(params, callback) {
        var MIN_LOADED_PERCENT = 15 // Primes the loaded percentage so user sees *something*

        if (this.hide_loaded_timeout !== null) {
            clearTimeout(this.hide_loaded_timeout)
            this.hide_loaded_timeout = null
        }

        this.loaded_percent = MIN_LOADED_PERCENT
        m.redraw()

        var old_config = () => {}
        if (has(params, 'config')) {
            old_config = params.config
        }

        params.config = (xhr) => {
            old_config(xhr)
            xhr.addEventListener('progress', e => {
                this.loaded_percent = Math.max((e.loaded / e.total) * 100, MIN_LOADED_PERCENT)
                m.redraw()

                if (this.loaded_percent === 100) {
                    this.hide_loaded_timeout = setTimeout(this.hide_loaded.bind(this), 420)
                }
            })
        }

        return this.request(params, callback)
    },

    hide_loaded: function() {
        this.loaded_percent = null
        m.redraw()
    }
}

module.exports = ApiHelper
