"use strict";

export default class ItemBase {
    constructor(wram, address) {
        var REQUIRED_METHODS = [
            this.getImgNumber,
            this.hasItem,
        ]

        if (!REQUIRED_METHODS.every(x => typeof(x) === "function")) {
            throw new TypeError("Must override method")
        }

        this.wram = wram
        this.address = address
    }

    getValue() {
        return this.wram.raw[this.address]
    }
}
