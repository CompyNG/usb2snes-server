"use strict";

import WramArea from '../WramArea'
import ProgressiveItem from './ProgressiveItem'
import SingleItem from './SingleItem'
import PendantItem from './PendantItem'

export default class ZeldaRam extends WramArea {
    constructor() {
        super(0xF000, 1280)

        this.items = {
            bow: new ProgressiveItem(this, 0x340, [0,48,49,49]),
            boomerang: new SingleItem(this, 0x341, 1),
            hookshot: new SingleItem(this, 0x342, 2),
            mushroom: new ProgressiveItem(this, 0x344, [4,46]),
            heart_pieces: new ProgressiveItem(this, 0x36b, [66,67,68,69]),
            green_pendant: new PendantItem(this, 4, 27),
            fire_rod: new SingleItem(this, 0x345, 5),
            ice_rod: new SingleItem(this, 0x346, 6),
            bombos: new SingleItem(this, 0x347, 7),
            ether: new SingleItem(this, 0x348, 8),
            quake: new SingleItem(this, 0x349, 9),
            blue_pendant: new PendantItem(this, 2, 28),
            red_pendant: new PendantItem(this, 1, 29),
        }

        this.name_cache = {
            raw_data: null,
            name: null
        }
    }

    name() {
        var name_slice = this.raw.slice(985, 993)
        if (
            (this.name_cache.raw_data !== null) &&
            (name_slice.length == this.name_cache.raw_data.length) &&
            (name_slice.every((v,i) => v === this.name_cache.raw_data[i]))
        ) {
            return this.name_cache.name
        }


        var name = "";
        for (var i = 0; i < name_slice.length; i += 2) {
            var character_code = (name_slice[i+1] << 8) | name_slice[i];

            if (character_code == 0x12a) {
                name += 'ツ'
            } else if (0x14a <= character_code && character_code <= 0x14f) {
                name += String.fromCharCode(character_code - 0x14a + 'A'.charCodeAt(0))
            } else if (0x160 <= character_code && character_code <= 0x16f) {
                name += String.fromCharCode(character_code - 0x160 + 'G'.charCodeAt(0))
            } else if (0x180 <= character_code && character_code <= 0x184) {
                name += String.fromCharCode(character_code - 0x180 + 'W'.charCodeAt(0))
            } else if (character_code == 0x189) {
                name += '-'
            } else if (character_code == 0x18c) {
                name += ' '
            } else if (character_code == 0x18e) {
                name += '~'
            } else {
                name += `?(${character_code})`
            }
        }

        this.name_cache.raw_data = name_slice
        this.name_cache.name = name

        return name
    }

    health() {
        var current_health = this.raw[0x340 + 45] / 8
        var total_health = this.raw[0x340 + 44] / 8

        var health_images = []

        var hearts_added = 0
        while (current_health > 0) {
            var health_image = (h => {
                if (h <= 0.25) return 67
                if (h <= 0.5) return 68
                if (h <= 0.75) return 69
                return 60
            })(current_health)

            health_images.push(health_image)
            current_health -= 1
            hearts_added += 1
        }

        while (hearts_added < total_health) {
            health_images.push(66)
            hearts_added += 1
        }

        return health_images
    }

    rupees() {
        return (this.raw[0x340 + 35] << 8) | (this.raw[0x340 + 34])
    }

    magic() {
        return {
            value: this.raw[0x340 + 46],
            max: 0x80
        }
    }
}
