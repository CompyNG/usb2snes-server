"use strict";

import SingleItem from './SingleItem'

export default class PendantItem extends SingleItem {
    constructor(wram, pendant_id, image) {
        super(wram, 0x374, image)

        this.pendant_id = pendant_id
    }

    hasItem() {
        return (this.getValue() & this.pendant_id) == this.pendant_id
    }
}
