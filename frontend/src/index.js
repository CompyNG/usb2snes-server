"use strict";
var m = require("mithril");

import ViewConnectToSnes from './views/ViewConnectToSnes';
import SnesWebsocket from './models/SnesWebsocket';

m.route.prefix("")

function routeToConnect() {
    m.route(document.body, "/", {
        "/": {
            render: function(vnode) {
                return m(ViewConnectToSnes)
            }
        },
    })
}

SnesWebsocket.connect().then(routeToConnect).catch(routeToConnect)
