import struct


class Patch(object):
    def __init__(self, address, data):
        self.address = address
        self.data = data


class IPS(object):
    def __init__(self):
        self.items = []

    @staticmethod
    def parse(data):
        self = IPS()

        if type(data) != bytes:
            raise RuntimeError('Need a bytes object to parse IPS from')

        if data[0:5] != b'PATCH':
            raise RuntimeError('Unexpected header')

        pos = 5
        while pos < len(data):
            if data[pos:pos+3] == b'EOF':
                break

            (offset0, offset1, offset2, size) = struct.unpack_from('>3BH', data, pos)
            offset = (offset0 << 16) | (offset1 << 8) | offset2
            pos += 5
            if size == 0:
                # RLE encoding
                (rle_size, value) = struct.unpack_from('>HB', data, pos)
                pos += 3
                self.items.append(Patch(offset, value * rle_size))
            else:
                self.items.append(Patch(offset, data[pos:pos+size]))
                pos += size

        if pos != len(data) and pos != (len(data) - 3):
            raise RuntimeError("Unexpected end of file ({}, {})".format(pos, len(data)))

        return self
